<?php
class TextToSpeech {
  public $lang;
  private $text;

  public function __construct($lang = "en") {
    $this->lang = $lang;
  }

  /**
   * Check if text is not empty and return the filename 
   */
  public function getFile($text) {
    $text = trim($text);
    if (!empty($text)) {
      $this->text = $text;
      return self::setFile();
    }
  }

  /**
   * Return the filename and write it if not exist
   */
  private function setFile() {
    $file = variable_get('file_public_path', conf_path() . '/files') . "/audio-" . md5($this->text) . ".mp3";
    if (!file_exists($file)) {
      file_put_contents($file, self::setSentence());
    }
    return $file;
  }

  /**
   * Set the sentence that will be spoken
   */
  private function setSentence() {
    $this->text =($this->text);
    if (strlen($this->text) > 100) {
      return self::splitSentence();
    }
    else {
      return self::getSentence($this->text);
    }
  }

  /**
   * Split the sentence for avoid max. lenght limit
   */
  private function splitSentence() {
    $mp3 = explode("\n", wordwrap($this->text, 100, "\n"));
    foreach($mp3 as $key => $val) {
      if (strlen($val) > 99) {
        $split = str_split($val, 99); 
        unset($mp3[$key]);
        foreach($split as $v) {
          $mp3[] = self::getSentence($v);
        }
      }
      else {
        $mp3[$key] = self::getSentence($val);
      }
    }
    return $mp3;
  }

  /**
   * Return the speech
   */
  private function getSentence($txt) {
    return file_get_contents("http://translate.google.com/translate_tts?tl={$this->lang}&q=" . urlencode($txt));
  }

}
