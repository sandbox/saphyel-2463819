# speaker
Drupal 7 module with HTML5 for add speech to selected fieldtexts

## Installation
Enable it and go to /admin/config/media/speaker for select fieldtext/s that will be shown audio player.

## Uninstall
Disable it and delete audio-MD5.mp3 files in /sites/default/files 
